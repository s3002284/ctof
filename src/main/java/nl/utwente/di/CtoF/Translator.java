package nl.utwente.di.CtoF;

public class Translator {
    public double translation(String celsius) {
        return (Double.parseDouble(celsius) * 1.8) + 32;
    }
}
