import nl.utwente.di.CtoF.Translator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTranslator {
    @Test
    public void testtrans1() throws Exception {
        Translator translator = new Translator();
        double price = translator.translation("50");
        Assertions.assertEquals(122.0, price, 0.0 , "translation");
    }
}
